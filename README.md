# Walter's Trivia Challenge

Trivia quiz offering customizable gameplay and over 2800 questions divided into over 80 categories covering various topics (with a slight bias in favour of history and geography but there are also questions about science, technology, animals, famous art, sci-fi, fantasy, food and many others).

## Changelog
* [Changelog](changelog.md)

## Documentation
* [Modding](documentation/modding/index.md)
* [Quick How To Quide](documentation/how_to/index.md)

![gameplay](documentation/images/gameplay.png)

![map](documentation/images/map.png)

![animal](documentation/images/animal.png)

![language](documentation/images/language.png)

![symbol](documentation/images/symbol.png)







/**
	Randomize array element order in-place.
	Using Durstenfeld shuffle algorithm (Fisher-Yates)
*/
function shuffle(array){
	for (let i = array.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[array[i], array[j]] = [array[j], array[i]];
	}
	return array;
}

function isLetter(c){
	return c.toLowerCase() != c.toUpperCase();
}

function isWord(c){
	if(!c) return false;
	return isLetter(c[0]) && isLetter(c[c.length-1])
}

function switchCase(c){
	if(!isLetter(c)) return c; 
	if(c.toLowerCase(c) === c) return c.toUpperCase(c);
	if(c.toUpperCase(c) === c) return c.toLowerCase(c);
}
	
	
	
//SUFLFES LETRELS IN WROD
function shuffle_letters(word, degree = 2){
	if( isNaN(degree)  ) degree = 2;
	
	//too short
	if(word.length < 4) return word;
	
	//the first...
	word = word.split("");
	let start = word.shift();
	//(or two first when the word is long)
	if(word.length > 6) start += word.shift();
	
	//...and the last letter stay on their positions
	const end = word.pop();
			
	//middle gets shuffled
	let mid;


	//first level - switches only two random letters
	if(degree == 1){		
		if(word.length > 2){ //at least 5-letter words, kinda redundant, dunno
			const place = Math.floor(Math.random() * (word.length - 2) );
			const temp = word[place];
			word[place] = word[place+1];
			word[place+1] = temp;
		};
		mid = word.join("");
		
	//second level - shuffles everything
	}else{
		mid = shuffle(word).join("");
	};
	
	
	return start + mid + end;
};




function tldr(text, degree = 2){
	const length = text.length;
	let random = Math.random();
	
	if(degree == 1) random = random * 2;
	
	if(
		(length > 400) 
		|| (length > 350 && random < 1.75) 
		|| (length > 310 && random < 1.50) 
		|| (length > 250 && random < 1.25) 
		|| (length > 190 && random < 1) 
		|| (length > 130 && random < 0.75) 
		|| (length >  90 && random < 0.50) 
	){
		

		//start
		let temp = 20 + Math.floor(Math.random() * 20);
		while (text[temp] != " " && temp > 0) temp--;
		const start = text.substring(0, temp)
		
		//mid
		let mid = "... blah... blah... ";
		if(length > 200) mid += "blah... ";
		if(length > 350) mid += "blah... ";
		if(length > 450) mid += "blah... ";
	
		//end
		temp = length - ( 20 + Math.floor(Math.random() * 20) );
		while (text[temp] != " " && temp < length - 1) temp++;
		const end = text.substring(temp, length)
	
		return start + mid + end;
		
	}else{
		return text;
	}
}
		
		

function random_capitals(text, degree = 2){ //TODO - IS FUCKING STUPID
	if( isNaN(degree)  ) degree = 2;
	
	let abundance = degree / 10;
	
	//abundace is roughly the ratio if random capitals 
	text = text.split("");
	abundance = text.length / (text.length * abundance);
	
	let index = Math.floor( (abundance/2) + (Math.random() * abundance) ); 
	while(index < text.length){
		text[index] = switchCase(text[index]);
		index += Math.floor( (abundance/2) + (Math.random() * abundance) ); 
	}
	
	/*
	for(let i = 0; i < text.length; i += Math.floor( 
		(abundence/2) + (Math.random() * abundance) 
	)){
		text[i] = switchCase(i);
	};
	*/
	return text.join("");
};
		
		
function to_roman(num){
	//https://www.selftaughtjs.com/algorithm-sundays-converting-roman-numerals/
	//TO DO - UNTESTED
	let result = '';
	let decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
	let roman = ["M", "CM","D","CD","C", "XC", "L", "XL", "X","IX","V","IV","I"];
	for (let i = 0;i<=decimal.length;i++) {
	// looping over every element of our arrays
		while (num%decimal[i] < num) {   
		// keep trying the same number until it won't fit anymore      
			result += roman[i];
			// add the matching roman number to our result string
			num -= decimal[i];
			// remove the decimal value of the roman number from our number
		}
	}
	return result;
}
	
export function distort(text, options = {
	shuffle: 0, //0, 1, 2
	tldr: false,
	capitals: 0, 
	roman: false,
}){
	
	//shortens the whole text
	if(options?.tldr) text = tldr(text);
	
	//divides into words	
	let words = text.split(" "); 
	
		//operations with words
		words = words.map( word => {
			if( !isNaN(word)   ){
				//console.log(word)
				if(options?.roman) word = to_roman(word);
			}else if( isWord(word)  ){
				if(options?.shuffle) word = shuffle_letters(word, options.shuffle);
			}
			return word;
		})
		
		
	text = words.join(" ")
	
	//shortens the whole text
	if(options?.capitals) text = random_capitals(text, options.capitals);



	
	return text;
}
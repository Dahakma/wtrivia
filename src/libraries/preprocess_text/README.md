# Preprocess text 


## Language 


```javascript
options.language
options.english
```



## Markdown lite

```javascript
options.markdown = true;
```

- transforms markdown-like syntax into html tags
- `# something` => `<h1>something</h1> -  # at the start of the line, whole line, ## => h2, etc., ###### => h6
- `**something**` => `<b>something</b>`
- `*something*` => `<i>something</i>`
- `~something~` => `<strike>something</strike>`
- `{something}` => `<small>something</small>`
- `^something else` => `<sup>something</sup> else` (^ - caret - alt+94)
- `^(something else)` => `<sup>something else</sup>`
- `∧something else` => `<sup>something</sup> else` (∧ wedge)
- `∧(something else)` => `<sup>something else</sup>`


## Trim

```javascript
options.trim = true;
```
- remove typos often caused by procedural generation of text and combining various strings (like multiple spaces or dots etc.)
- `abc,,   qwe;;;` => `abc, qwe;` - replace multipled `,`, `;` and ` `
- `abc , ;` => `abc,;` - removes ` ` before `,` or `;`
- `abc,qwe;yxc` => `abc, qwe; yxc` - forces ` ` behind `,` and `;`; 		
	ignores `.`
	ignores when digits on both sides (`1,000` => `1,000)
- `  abc  ` => `abc` - outer trim


## Replace

```javascript
options.replace = undefined; //default
options.replace{
	symbol,
	fce,
}
```

- uses the `fce` to replace words following the `symbol`


```javascript
const text = "@name has a cat. ";

function replace(key){
	if(key === "name") return "John";
}

preprocess_text(text, {
	replace: {
		symbol: "@"
		fce: replace,
	},
}); //"John has a cat. "
```


## Distort 

```javascript
options.replace = undefined; //default
options.distort{
	shuffle,
	tldr,
	capitals,
	roman,
}
```

- turns text into mess
- `shuffle` - shuffles letters 
	- `0` - no
	- `1` - two letters
	- `2` - all
- `tldr` - shortens text, replaces with - `blah...`
- `capitals` - randomly capitalise letters 
	- `0` - no
	- `1` - few
	- `2` - many
- `roman` - replaces numbers with Roman numbers 





\d+?[_\.\d]+@\w+

(\d+?[_\.\d]+)@(\w+)

digit followed by digit or dot or underscore
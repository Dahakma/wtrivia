import {distort} from "./distort";
import {units} from "./units";

/**
 *	preprocesses text
 * @param {string} text - text input
 * @param {Object} options - options for transformation
 * @param {Object|undefined} [options.markdown] - change markdown-like tags into html
 * @param {boolean} [options.markdown.block = true] - block elemens like <h1>
 * @param {boolean} [options.markdown.numbers = false] - transform all numbers to local
 * @param {boolean} [options.trim = true] - remove weird diacritic (assumed typos) (e.g. " abc ,,  de;gh.  " => "abc, de; gh.")
 * @param {string} [options.language = navigator.language] - default could be manually overrriden ("en-US"; "en-GB"; "cs-CZ"; "de-DE" etc.)
 * @param {string} [options.english = "uk"]  -  "uk" or "us" -  prefer UK or US english ("metre" or "meter"); defaultly "uk" unless the language is "en-US"
 * @param {Object|undefined} [options.replace = undefined] - replaces word following selected symbol (e.g. "@name" => "Katarína")
 * @param {string} options.replace.symbol - symbol denoting following 
 * @param {function} options.replace.fce - replacing function, input is the word
 * @param {Object|undefined} [options.distort = undefined] - turns text into mess
 * @param {integer} options.shuffle - shuffle letters in word (0 - no; 1 - little; 2 - all)
 * @param {boolean} options.tldr - replace middle of text with "blah..."
 * @param {integer} options.capitals - randomly capitalise letters (0 - no, 10 - max)
 * @param {boolean} options.roman - replaces  numbers with roman
 * @param {Object|undefined} [options.units = undefined] - handles measurement units ("1@mi" => "0.621 kilometres")
 * @param {string} options.units.symbol - symbol denoting units (could be same as the replace.symbol)
 * @param {string} [options.units.system = "si"] - "si" or "us" - US customary
 * @param {boolean} [options.units.full = false] - "m" or "metre"
 */
export function preprocess_text(text, options = {
	trim: true,
	markdown: {
		block: true,
		numbers: false,
	},	
	replace: undefined,
	distort: undefined,
	units: undefined,
}){
	
	if(typeof text !== "string"){
		return undefined;
	}



	options.language ??= window.navigator.language; 
	options.english ??= options.language === "en-US" ? "us" : "en";

	if(options?.trim) text = trim(text);
	if(options?.units) text = units(text, options.units, options); //before markdown & replace
	if(options?.markdown) text = markdown_lite(text, options.markdown, options);
	
	if(options?.replace) text = replace(text, options.replace);
	if(options?.distort) text = distort(text, options.distort);

	return text;
}


//const test = "John has a lovely dog. 78_700@ok Beautiful maiden goes to a fair. ";
/*
const test = `
100@m
100@cm
100@km
	0.09290304@ft2 = 1 m2
	10@mi = 16.0934 km
	212@f = 100 c
	100@f = 37.77 c
	35.3147@ft3 = 1 m3
	1@ft 10@ft
	100@ft2 = 1076 m2
`;
 

console.warn(  preprocess_text(test, {
	trim: true,
	units: {
		symbol: "@",
		system: "us",
		//language: "us",
		full: true,
	},
}));
*/

/*
const test = "9879 1_000_000 4_456 4.456_123";

console.error(  preprocess_text(test, {
	//language: "en-US", 
	markdown: true,
}));
 
 console.log("d");
*/


//REPLACER 
function replace(text, options){
	//input = input.replace(/@(\w+)/gm, smail);

	const {fce, symbol} = options;
	if(typeof fce !== "function" || typeof symbol !== "string") return text;
	
	function exec(match, p1){
		return fce(p1);
	}
	const rex = new RegExp(`${symbol}(\\w+)`, `gm`); //TODO - ADD ESCAPE
	return text.replace(rex, exec); 
}



//MARKDOWN 
import {stringify_number} from "./util";

function markdown_lite(data, options, all_options){


	
if(options.block){
	// headlines H1 - H6
	for(let i = 6; i > 0; i--){
			function exec(match, p1){
					return `<h${i}>${p1}</h${i}>`;
			}
			const rex = new RegExp(`^#{${i}}(.*)`, `gm`)
			data = data.replace(rex, exec); 
	}
}




	//format numbers
	function format_numbers(text){
		function exec(match, group){
			const newest = group.replaceAll("_", "");
			if(newest === group && !options.numbers) return group; //nothing to change 
			return stringify_number(+newest, all_options);
		}
		const rex = new RegExp(`(\\d[_\\.\\d]*)`, `gm`); //TODO - ADD ESCAPE
		return text.replace(rex, exec); 
	}	
	data = format_numbers(data);

/*
	//VERSION WITH NEGATIVE LOOKBEHIN WHICH DOES NOT WORK IN SAFARI

	function between(fce, start, end = start){
		function exec(match, p1){
			return fce(p1);
		}
		// (?<!\\) - not start with "\" - used as escape
		// \ in strings are disappearing so they have to be doubled
		const rex = new RegExp(`(?<!\\\\)${start}(.*?)${end}`, `gm`)
		data = data.replace(rex, exec); 
	}
	
	between(a => `<b>${a}</b>`, `\\*{2}`); // **something** to <b>something</b>
	between(a => `<i>${a}</i>`, `\\*{1}`); //  *something* to *something</i>
	between(a => `<strike>(${a})</strike>`, `~`); //  ~something~ to <strike>something</strike>
	between(a => `<small>(${a})</small>`, `{`, `}`); // {something} to <small>(something)</small>

	data = data.replace(/(?<!\\)\^(\w+)/gm,`<sup>$1</sup>`); // ^something to <sup>something<sup>
	
*/
	const escape = ``; //`\?` //initial escape sequence - atm not defined because / causes issues of being escape character in js too and I dont know what else would be better and whether the escape character is even needed
	
	function between(fce, start, end = start){
		function exec(match, p1){
			//if( match.startsWith(escape) ) return p1; //TODO - REXEX?
			return fce(p1);
		}
		
		const rex = new RegExp(`${escape}${start}(.*?)${end}`, `gm`)
		data = data.replace(rex, exec); 
	}
	
	function behind(fce, start){
		function exec(match, p1){
			//if( match.startsWith(escape) ) return p1; //TODO - REXEX?
			return fce(p1);
		}
		
		// anything following closed in "()"
		const rex_long = new RegExp(`${escape}${start}\\((.*?)\\)`, `gm`)
		data = data.replace(rex_long, exec); 
		
		// only following single word (or number)
		const rex_short = new RegExp(`${escape}${start}(\\w*)`, `gm`)
		data = data.replace(rex_short, exec); 
	}

/*
	MARKDOWN-LITE
	`**something**` => `<b>something</b>`
	`*something*` => `<i>something</i>`
	`~something~` => `<strike>something</strike>`
	`{something}` => `<small>something</small>`
	
	`^something else` => `<sup>something</sup> else` (^ - caret - alt+94)
	`^(something else)` => `<sup>something else</sup>`
	`∧something else` => `<sup>something</sup> else` (∧ wedge)
	`∧(something else)` => `<sup>something else</sup>`
	
*/

	// \ in strings are disappearing so they have to be doubled
	between(a => `<b>${a}</b>`, `\\*{2}`); // **something** to <b>something</b>
	between(a => `<i>${a}</i>`, `\\*{1}`); //  *something* to *something</i>
	between(a => `<strike>(${a})</strike>`, `~`); //  ~something~ to <strike>something</strike>
	between(a => `<small>(${a})</small>`, `{`, `}`); // {something} to <small>(something)</small>


//between(a => `<b>${a}</b>`, `#`); // **something** to <b>something</b>


	between(a => `<small>(${a})</small>`, `†`); //  †something† to <small>(something)</small>
	data = data.replaceAll("†",""); //TODO TEMPFIX!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	//data = data.replace(/(†)\s*$/gm, ``); // removes tag † at the end of the line notifying events in 
	
	//(209 000 m^3/s) is (rivers) TODO  check if works
	// longest {3 692 km} European river is 
//	
	
//between(a => `<small>(${a})</small>`, `†`, `†`); // †something† to <small>(something)</small>
//data = data.replaceAll("†",""); //TODO TEMPFIX!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	
	behind(a => `<sup>${a}</sup>`, `[\\^\\∧]`);
	behind(a => `<sup>${a}</sup>`, `˄`); //TODO
	behind(a => `<sub>${a}</sub>`, `˅`);
	return data;
}





/*
	`abc,,   qwe;;;` => `abc, qwe;` - replace multipled `,`, `;` and ` `
	`abc , ;` => `abc,;` - removes ` ` before `,` or `;`
	`abc,qwe;yxc` => `abc, qwe; yxc` - forces ` ` behind `,` and `;`; ignores `.`; ignores when digits on both sides (`1,000` => `1,000)
	`  abc  ` => `abc` - outer trim
*/

function trim(input){
	if(!input || typeof input !== "string") return ``; //redundant
	let output = input;

	 // "abc,,   qwe;;;" => "abc, qwe;";
	//TODO - THIS VERSION INCLUDE ALL WHITESPACE (inc line break) //output = output.replace(/[,]{2,}|[;]{2,}|\s{2,}/gm, a => a[0] );
	//output = output.replace(/[,]{2,}|[;]{2,}|\ {2,}/gm, a => a[0] );
	
//TODO ; REMOVED TEMPFIX
output = output.replace(/[,]{2,}|\ {2,}/gm, a => a[0] );
//Krak&#243;w

	// "abc , qwe; " =>  "abc, qwe; "
	output = output.replace(/\s+,/gm, `,`);  //TODO - aren't those regexes redundant? 
	//output = output.replace(/\s+([,;])/gm, (match, p1) => p1);
//TODO ; REMOVED TEMPFIX
	output = output.replace(/\s+([,])/gm, (match, p1) => p1);

	// "abc,qwe;yxc" => "abc, qwe; yxc" 
	//output = output.replace(/([^\d\W]+)([,;])(\w+)|(\w+)([,;])([^\d\W]+)/gm, (match, p1, p2, p3, p4, p5, p6) => p1 ? `${p1}${p2} ${p3}` : `${p4}${p5} ${p6}` );
//TODO ; REMOVED TEMPFIX
output = output.replace(/([^\d\W]+)([,])(\w+)|(\w+)([,])([^\d\W]+)/gm, (match, p1, p2, p3, p4, p5, p6) => p1 ? `${p1}${p2} ${p3}` : `${p4}${p5} ${p6}` );
	/*
		¨[^\d\W] will match any non-digit and (non-non-word) i.e. a word character
		([^\d\W]+),(\w+)  matches "asdf,123"
		(\w+),([^\d\W]+) matches "123,asdf"
		"123,123" are ignored
		"asdf,asdf" are matched always
	*/

	// "  abc   " => "abc";
	output = output.trim(); 


	//TODO ASAP Lookbehind issues
	//output = output.replace(/(?<=\w)[,;](?=\w)/gi, `$& `); // "abc,qwe" => "abc, qwe"

	//output = output.replace(/[.]\s[.]/gi, a => a[0] ); // "abc. ." => "abc."; //TODO - not sure if bright idea 
	//output = output.replace(/(?<=\w\.)[a-z]/g, a => ` ${a.toUpperCase()}` ); // "abc.qwe " => "abc. Qwe";
	
	return output;
}


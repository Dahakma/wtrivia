//TODO - add the rest of the library, lol
export const cap = function(string) {
  if (!string || typeof string !== "string") return "";
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export const clamp = function(input, min, max) {
  return input < min ? min : input > max ? max : input;
}

export const clone = function(input){
	return JSON.parse(JSON.stringify(input));
}


//TODO - this is tempfix 
export const update = function(original = {}, altered = {}){
	const chimera = clone(original);

	Object.keys(altered).forEach( key => {
		if(typeof altered[key] === "object"){
			chimera[key] = update(chimera[key], altered[key]);
		}else{
			chimera[key] = altered[key]
		}
	})
	
	return chimera;
}


//TODO TODO TODO
export const retrieve = function(original, key){
	let current; 
	try {
		current = update(  original, JSON.parse( localStorage.getItem(key) )  );
	} catch (error) {
		current = clone(original);
	}

	try {
		localStorage.setItem(key, JSON.stringify(current));
		//current = update(  original, JSON.parse( localStorage.getItem(key) )  );
	} catch (error) {
	}

console.error(current);
	return current;
}




/*
let a = {b: {c: 4, cc: 4444, d: {k: [1,2,3]}}, a: 45}
let b = {b: {c: 5, ccbb: 7878787, d : {o: 8787, k: [1,5]}}, d: 787}


console.error( update(a, b) );


console.error( update(b, a) );

console.error( update({}, a) );
console.error( update(b, {}) );

console.log(a)
console.log(b)

*/
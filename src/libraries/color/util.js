/**
	seed-based random number 
	see: http://indiegamr.com/generate-repeatable-random-numbers-in-js/ 
*/
export const rg = {
	number: 0,
	i(...arg){
		//console.trace();
		arg = arg.map( a => {
			if(a === undefined){
				 return new Date().getTime();
			}else if(typeof a === "string"){
				return +[...a].map( c => c.charCodeAt() ).join('');
			}else{
				return a;
			}
		})
		this.number = arg.reduce( (a,c) => a + c );
		return this.g;
	},
	get date(){
		return  new Date().getTime();
		//return a;
	},
	get g(){
		this.number = (this.number * 9301 + 49297) % 233280;
		return this.number / 233280;
	},
	integer(min, max){
		return Math.round( min + this.g * (max - min) );
	},
	range(min, max){
		return  min + this.g * (max - min);
	},
	between(min, max){
		return  min + this.g * (max - min);
	},
	array(array){
		return  array[ Math.floor( array.length * this.g ) ];
	},
	tweak(input, adjust){
		if( !(input * adjust) || isNaN(input * adjust) ) return input;
		return input - adjust + (2 * adjust * this.g);
	}
};
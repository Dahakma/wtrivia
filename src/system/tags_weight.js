import {
	tags_key,
	constants,
} from './variables';
import tags from 'Data/tags.json';

const weight_default = {}; 
Object.keys(tags).filter( key => !tags[key].hidden ).forEach( key => weight_default[key] = constants.tag_weight ); 

export const tags_weight = {
	load(){
		let tags_weight = null;
		try {
			tags_weight = JSON.parse( localStorage.getItem(tags_key) );
		} catch (error) {
		}
		return tags_weight === null ? weight_default : tags_weight;
	},

	save(tags_weight){
		try {
			localStorage.setItem( tags_key, JSON.stringify(tags_weight) );
		} catch (error) {
		}
	},
}


/*
	default values are overriden by the values from localStorage 
	- in case values in the localStorage are not initialised (game loaded for the first time)
	- or in the case not all values are present in the localStorage (some tags were newly added)
*/
const weight_actual = {   
	...weight_default,  
	...(tags_weight.load()),
}

tags_weight.save(weight_actual);
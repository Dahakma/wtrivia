import {
	setting_key,
	setting_default,
} from './variables';
import {retrieve} from 'Libraries/dh/index';

import { writable, get } from 'svelte/store';
const colors = ["background", "background_2", "highlight", "text"];


//changes css style of the page with current colors
function apply_colors(setting){
	const root = document.querySelector(":root");
	colors.forEach( key => root.style.setProperty(`--${key}`,  setting[key]) );
}


//creates svelte store 
function create_setting(input){
	const { subscribe,  update } = writable(input);

	return {
		subscribe,
		update,
		
		//changes one variable
		change: (key, value) => update(setting => {
			setting[key] = value;
			if(colors.includes(key)) apply_colors(setting);
			return setting;
		}),
		
		//resets to default
		reset: () => update(setting => {
			setting = {...setting_default};
			return setting;
		}),
		
		//resets colors to default
		reset_colors: () => update(setting => {
			colors.forEach( key => setting[key] = setting_default[key] );
			apply_colors(setting);
			return setting;
		}),
		
		//saves setting to localStorage
		save: () => update(setting => {
			localStorage.setItem(setting_key, JSON.stringify(setting));
			return setting;
		}),
	}
}





//loads settings from localStorage or uses defalut 
export const setting = create_setting( retrieve(setting_default, setting_key)  ); 
apply_colors( get(setting) );


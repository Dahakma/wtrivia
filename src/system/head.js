import {
	debug,
	head_key,
	head_default,
} from './variables';
import {constants} from 'System/variables';
import { writable } from 'svelte/store';
import {categories, tagged} from './lists/categories';
import {questions, ninja_swap} from './lists/questions';
import {clone, retrieve} from 'Libraries/dh/index';



//NEW ROUND (clears active variables, loads categories) - new_game & next
function new_round({display, progress, mode, game, round}){
	if(progress){
		head.save();
	}else{
		progress = true;
	}

	round = clone(head_default.round);
	
	display = "roundstart";

	const  how_many = (()=>{
		if(mode.offered === "mix") return mode.questions;
		if(mode.gameplay === "survival") return constants.offered_categories.survival;
		return mode.rounds + 1 + Math.ceil(mode.rounds / constants.offered_categories.standard_extra);
	})();
	
	if(mode.offered === "mix"){ //new categories have to be mixed every round 
		game.categories = categories(
			how_many,
			game.seed + game.round,
		);
	}else if(mode.gameplay === "standard" && game.categories.length){ //categories already initated   (in standard game they are initialized only at the beginning)
		//empty 
	}else{
		if(mode.offered === "tags" ){
			game.categories = tagged(
				how_many,
				game.seed + game.round,
			);
		}else if(mode.offered === "cats" ){
			game.categories = categories(
				how_many,
				game.seed + game.round,
			);
		}
	}

	return {
		display,
		progress,
		game,
		round,
	};
}



//NEXT - decides what happens next (end of game, next question, next round)
function next({display, mode, game, round}){
		
	round.question++;

	//STANDARD
	if(mode.gameplay === "standard"){
		if(round.question >= mode.questions){
			game.round++;
			//end game
			if(game.round >= mode.rounds){ 
				display = "end";
			//new round 
			}else{ 
				display = "roundstart";
			}
		//new question
		}else{
			display = "question";
		}
	
	//SURVIVAL
	}else if(mode.gameplay === "survival"){
		//dead - end of game
		if(mode.survival_style === "game"){
			if(game.wrong > mode.survival_wrong){
				display = "end";
			}
		}else if(mode.survival_style === "round"){ 
			if(round.wrong > mode.survival_wrong){
				display = "end";
			}
		}

		//end of round X next question
		if(display === "end"){
			//nothing; the end
		}else if(round.question >= mode.questions){
			game.round++;
			display = "roundstart";
		}else{
			display = "question";
		}

		// danger zone? 
		game.danger_zone = false;

		if(head.display === "end"){
			//nothing; the end
		}else if(mode.gameplay === "survival" && mode.survival_wrong === 0){
			game.danger_zone = true;
			//somebody likes to live dangerously
		}else if(mode.survival_style === "game"){
			if(game.wrong === mode.survival_wrong){
				game.danger_zone = true;
			}
		}else if(mode.survival_style === "round" && display !== "roundstart"){ 
			if(round.wrong === mode.survival_wrong){
				game.danger_zone = true;
			}
		}	
	}

	return {
		display,
		game,
		round,
	};
}


function save(head){
	const temp = {
		...head,
		game_display: (head.progress || head.display === "reload") ? "reload" : "", //saved when game in progress; or again saved save made when  game was in progress
		progress: false, //when loaded, the progress will be false
		display: "menu",
	};
	localStorage.setItem(head_key, JSON.stringify(temp));
}


const game_displays = ["roundstart", "answer", "question"];

function create_head(input){
	const { subscribe,  update  } = writable(input);

	return {
		subscribe,
		update,

		//what "screen" is displayed 
		display: (what) => update(head => {

			if(what === "menu" && game_displays.includes(head.display) ){
				head.game_display = head.display;
			}
			head.display = what;
			
			if(head.display === "reload"){
				head = {...head, ...new_round(head)};
			};

			return head;
		}),

	

		//changes setting related to the current game
		mode: (new_mode) => update(head => {
			head.mode = new_mode;
			return head;
		}),


		save: () => update(head => {
			save(head);
			return head;
		}),
		

	//NEW GAME - clears variables and runs new round
		new_game: (seed) => update(head => {
			
			head.game = clone(head_default.game);
			head.game.seed = seed ?? (new Date()).getTime(); 
			head.game.danger_zone = head.mode.gameplay === "survival" && head.mode.survival_wrong === 0;
			head.progress = true;
			
			head = {...head, ...new_round(head)};

			return head;
		}),
		

	//LOAD QUESTIONS  
		load_questions: (category_index, selection_index, extra) => update(head => {
			if(selection_index !== undefined) head.game.selected.push(selection_index); //category was already selected 
			head.round.questions.length = 0; //clears previous
			head.round.promised.length = 0; //clears previous

			const pending = questions(
					category_index, //index of category or categories
					head.mode.questions, //how many
					head.game.seed + head.game.round, //seed 
					extra,
			);
			const already_tried = pending.map( a => a );

			function load(a, index){
				return new Promise( (good, bad) => {
					let attempts = 5;
					function check_image(a){
						if(!a.img) return good(a);
						const image = new Image();
						image.src = a.img;
						image.onload = () => {
							good(a);
						}
						image.onerror = () => {
							/* eslint-disable no-console */
							console.warn(`WARNING: picture cannot be loaded! [cat: ${a.category}; lev: ${a.level}; que: ${a.question}] [index:   ${index}]`);
							console.warn(a.img);
							/* eslint-enable no-console */

//TODO - all failed attemts untested!!!
							if(attempts-- == 0) return bad(a); 
							
							//when image cannot be loaded, tries to switch the question for another one
							already_tried.push(a);
							const ninja = ninja_swap(a, already_tried, head.game.seed + already_tried.length);
							head.round.questions[index] = ninja;
							check_image(ninja);
						}
					}
					
					check_image(a);				
				})
			}    

			pending.forEach( (a,i) => {
					head.round.questions.push( a );
					head.round.promised.push( load(a,i) );
			});

			head.display = "question"; 
			return head;
	
		}),
		
		
	//ANSWER 
		answer: (choice_index) => update(head => {
			//no answer (error)
			if(choice_index === null){
				//not counted among totals nor right nor wrong
							//head.active.was_correct = false;
				head.display = "answer";
				return head;
			}	
			//correct
			if(choice_index === head.round.questions[ head.round.question ].correct_choice){
				head.game.total++;
				head.game.right++;
				head.round.total++;
				head.round.right++;
				head.round.was_correct = true;
			//incorrect 
			}else{
				head.game.total++;
				head.game.wrong++;
				head.round.total++;
				head.round.wrong++;
				head.round.was_correct = false;
			}
			head.display = "answer";
			return head;
		}),
		
	
	//NEXT -  resolves next question X next round X end 
		next: () => update(head => {
			/* eslint-disable no-console */
				if(debug){
					console.log("next:");
					console.log(head);
				}
			/* eslint-enable no-console */
			

			head = {...head, ...next(head)};

			if(head.display === "roundstart"){
				head = {...head, ...new_round(head)};
			}else if(head.display === "end"){
				head.progress = false;
				head.game_display = "";
			}

			return head;
		}),
		

	}
}




//ACTUALLY INITIATE 
export const head = create_head( retrieve(head_default, head_key)  ); 

import {
	title,
	version,
	version_key,
	debug,
	setting_default,
} from './variables';

export {setting} from './setting';
export {head} from './head';

export {tags_weight} from './tags_weight';

localStorage.setItem(version_key, version); //BEWARE - after setting and head!


/* eslint-disable no-console */
	console.log(`%c${title} - version ${version}`, `
		color: ${setting_default.highlight};
		background-color: ${setting_default.background}; 
		font-size: larger;
	`);
/* eslint-enable no-console */



//KEYBOARD
import {head} from './head';
export const keyboard = {
	items: [], 
	add(index, fce){
		this.items[index] = fce;
	},
	reset(){
		this.items.length = 0;
	},
}

document.addEventListener("keydown",  ({key}) => {
	if(typeof keyboard.items?.[key] === "function"){
		keyboard.items[key]();
		keyboard.reset();
	}
	if(key === "Escape"){
		head.display("menu");
		keyboard.reset();
	}
});
		
		
		
		
		
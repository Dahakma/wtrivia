import cat from 'Questions';
import tags from 'Data/tags.json';
import {setting} from 'System/initiate';
import { get } from 'svelte/store';
import {tags_weight} from 'System/initiate';
import {category_weight} from './category_weight';


/**
 * returns category weight (how big is the chance that this category will be offered to the player 
 * @param {string} tag - id of the tag
 * @returns {Object} weight 
 * @returns {number} weight.weight - value 
 * @returns {string} weight.report - explanation for debugging purposes
 * @returns {Object[]} weight.actual_cats - valid available categories with the tag
 * @returns {number} weight.actual_cats[].index - index of category
 * @returns {number} weight.actual_cats[].weight - weight of category
 * @returns {number} weight.actual_cats[].sum - number of questions in category
 */
export const tag_weight = function(tag){
	const user_weight = tags_weight.load();
	const report = [];
	const actual_cats = [];
		
		//hidden tag
		if( tags[tag] === undefined  ){
			return {
				weight: 0,
				report: `0 - not valid tag`,
				actual_cats,
			};
		}

		//hidden tag
		if( tags[tag].hidden  ){
			return {
				weight: 0,
				report: `0 - hidden tag - not offered during gameplay`,
				actual_cats,
			};
		}

		//tag banned by user
		if( user_weight[tag] === 0 ){
			return {
				weight: 0,
				report: `0 - tag banned by user`,
				actual_cats,
			};
		}

		//questions with maps are disabled
		if(  !get(setting).enable_maps && (tag === "map" || tag === "xmap") ){
			return {
				weight: 0,
				report: `0 - maps are disabled`,
				actual_cats,
			};
		}

		//requires online content but it is disabled or there is no internet connection 
		if( tag === "images" ){
			if( !get(setting).enable_online ){
				return {
					weight: 0,
					report: `0 - content (images) downloaded from the Internet is disabled`,
					actual_cats,
				};
			}else if(!window.navigator.onLine){
				return {
					weight: 0,
					report: `0 - no Internet conection is detected and the downloaded content (images) is not available`,
					actual_cats,
				};
			}
		}


		//categories with the tag
		cat.forEach( (cathy, index) => {
			if( cathy.tags.includes(tag) ){
				const weight = category_weight(index).weight;
				if( weight > 0 ){
					actual_cats.push({
						index, 
						weight,
						sum:  cathy[0].length + cathy[1].length + cathy[2].length,
					});
				}
			}
		});
		const sum = actual_cats.reduce( (tot, a) => tot + a.sum, 0 );

		//no available valid categories 
		if(!actual_cats.length){
			return {
				weight: 0,
				report: `0 - no valid categories associated with the tag`,
				actual_cats,
			};
		}

	
	//base value
		let weight =  2; //base chance
		report.push(`&nbsp${weight} - base weight`);
	
		if(sum > 200){ 
			weight += 3;
			report.push(`+3 - more than 200 valid questions`);
		}else if(sum > 100){
			weight += 2;
			report.push(`+2 - more than 100 valid questions`);
		}else if(sum > 50){
			weight++;
			report.push(`+1 - more than 50 valid questions`);
		}
	
		if(actual_cats.length > 6){ 
			weight += 3;
			report.push(`+3 - more than 6 valid categories`);
		}else if(actual_cats.length > 4){
			weight += 2;
			report.push(`+2 - more than 4 valid categories`);
		}else if(actual_cats.length > 2){
			weight++;
			report.push(`+1 - more than 2 valid categories`);
		}



	//tag boost
		const tag_boost = user_weight[tag]/10;
		weight = Math.round(weight * tag_boost);
		report.push(`x ${tag_boost} - user-adjusted boost`); //TODO!!!
		

	report.push(`= ${weight}`);


	const master_tag =  ["map", "xmap", "images"].includes(tag) ? tag : undefined;
		

	return {
		weight,
		report: report.join("<br>"),
		actual_cats,
		master_tag,
	};
}



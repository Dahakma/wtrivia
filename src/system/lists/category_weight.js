import cat from 'Questions';
import tags from 'Data/tags.json';
import {setting} from 'System/initiate';
import { get } from 'svelte/store';
import {rg} from 'Random';
import {tags_weight} from 'System/initiate';



/**
 * returns category weight (how big is the chance that this category will be offered to the player 
 * @param {number} index - index of the category
 * @returns {Object} weight 
 * @returns {number} weight.weight - value 
 * @returns {string} weight.report - explanation for debugging purposes
 * @returns {string} weight.master_tag - "images", "map", "xmap" or "undefined"
 * @returns {string[]} weight.actual_tags - tags without conflicting and invalid ones
 */
export const category_weight = function(index){
	const cathy = cat[index];
	const report = [];
	const user_weight = tags_weight.load();

	//SYSTEM
		//remove finals - should not be here, used only in WWBB
		if(cathy.fin) return 0;
	
	
	//MASTER TAG & ACTUAL TAGS - basically the category cannot have both "map" and "xmap" tags and here is randomy decided which one it will be
		let actual_tags = cathy.tags.filter( tag => Object.keys(tags).includes(tag) ); //only real, defined tags, not everything
		const master_tag = (()=> {
			const map = cathy.tags.includes("map");
			const xmap = cathy.tags.includes("xmap");
			const images = cathy.tags.includes("images");

			if(images){
				return "images";
			}else if(map && xmap){ //BEWARE - assumes weight of at least one of the tags is possitive 
				{
					const master = ["map","xmap"][rg.weighted([user_weight.map, user_weight.xmap])];
					if(master === "map"){
						actual_tags = actual_tags.filter( tag => tag !== "xmap" );
					}else{
						actual_tags = actual_tags.filter( tag => tag !== "map" );
					}
					return master;
				}
			}else if(map){
				return "map";
			}else if(xmap){
				return "xmap";
			}
		})();


		
	//DISABLED CATEGORY LEVEL
		//category has one of user-baned tags
		const banned = actual_tags.filter( key => user_weight[key] <= 0 );
		if(banned.length){
			return {
				weight: 0,
				report: `0 - the category has user-banned tags (${banned.join("; ")})`,
				actual_tags,
			};
		}
		
		//questions with maps are disabled
		if(  !get(setting).enable_maps && ( actual_tags.includes("map") || actual_tags.includes("xmap") )  ){
			return {
				weight: 0,
				report: `0 - has tag "map" but maps are disabled`,
				actual_tags,
			};
		}
		//requires online content but it is disabled or there is no internet connection 
		if(  actual_tags.includes("images") ){
			if( !get(setting).enable_online ){
				return {
					weight: 0,
					report: `0 - has tag "images" but content (images) downloaded from the Internet is disabled`,
					actual_tags,
				};
			}else if(!window.navigator.onLine){
				return {
					weight: 0,
					report: `0 - has tag "images" but no Internet conection is detected and the downloaded content (images) is not available`,
					actual_tags,
				};
			}
		}




	//BASE VALUE
		let weight =  4; //base chance
		report.push(`&nbsp${weight} - base weight`);

		//NUMBER OF QUETIONS
		const sum = cathy[0].length + cathy[1].length + cathy[2].length;
		if(sum > 40){ 
			weight += 3;
			report.push(`+3 - more than 40 questions`);
		}else if(sum > 30){
			weight += 2;
			report.push(`+2 - more than 30 questions`);
		}else if(sum > 20){
			weight++;
			report.push(`+1 - more than 20 questions`);
		}
	

	//NEWEST
		if(  actual_tags.includes("newest") ){
			weight += 5;
			report.push(`+5 - has tag "newest"`); //TODO!!!
		}



	//TAG BOOST
		let tag_boost = 1;
		actual_tags.forEach( key => {
			//if( user_weight[key] ) boost += (user_weight[key] - 1)
			if( user_weight[key] ) tag_boost *= user_weight[key]/10;
		});

		weight = Math.round(weight * tag_boost);
		report.push(`x ${tag_boost} - multiplier based on tags`); //TODO!!!
		

	report.push(`= ${weight}`);

	return {
		weight,
		report: report.join("<br>"),
		actual_tags,
		master_tag,
	};
}

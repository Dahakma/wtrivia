export const title = "Walter's Trivia Challenge";
export const version = "0.9.6"; //window.__VERSION__; //TODO - doesnt work during build
export const debug = true;
export const head_key = "wtrivia_game";
export const setting_key = "wtrivia_setting";
export const version_key = "wtrivia_version";
export const tags_key = "wtrivia_tags";
export const stats_key = "wtrivia_stats";

export const constants = {
	tag_weight: 10,
	rounds: {
		min: 1,
		max : 10,
	},
	questions: {
		min: 1,
		max : 10,
	},
	survival_wrong: {
		min: 0,
		max : 10,
	},
	offered_categories: { // number of categories offered at the start of the round
		survival: 3,
		standard_extra: 3, // for each X rounds in standard mode will be added one extra category  
		// 3 rounds => 3 + 1 + Math.ceil(3 / 3) = 5 categories
	},
}

	
//HEAD
export const head_default = {
	display: "menu",	// what is currently displayed ("menu", "question" etc.)
	game_display: "", //the last screen displaying the game itself (enables continue button)
	progress: false, // game in active progress 
	mode: { // related to the setting of the current game
		//categories: 1, //TODO????
		questions: 4, // questions offered in one round 
		rounds: 5,  // rounds in "standard" game
		gameplay: "standard", //type of game
		/*
			"standard" - predetermined number of rounds
			"survival" - until you make "survival_wrong" mistakes
		*/
		survival_wrong: 3, // how many mistakes could be made in survial mode
		survival_style: "game",  //subtype of survival game
		/*
			 "round" - maximal mistakes in one round 
			"game" - maximal mistakes in whole game
		*/
		offered: "cats",  // what is offered at the beggining of the round
		/*
			"cats" - categories
			"tags" - randomly picked from categories with tag
			"mix" - randomly picked from random categories
		*/
	},
	game: { // related to the current game in progress
		
		danger_zone: false, // the next wrong answer will end the game (survival mode)
		seed: undefined, // seed used for randomization 

		right: 0, // correct answers
		wrong: 0, // incorrect answers 
		total: 0, // total answers 

		categories: [], // indexes of offered categories
		selected: [], // indexes of indexes of offered categories that were already selected (and can't be selected again)
	
		round: 0, //index of active round
	},
	
	round: { // related to the active round in progress
		question: 0, //index of active questions 
		right: 0, // correct anwers in this round
		wrong: 0, // incorrect answers in this round
		total: 0, // total answers in this round
		was_correct: false, // the previous answer was answered correctly 

		questions: [], // questions
		promised: [], // questions in form of promises (because of loading of images)
	},
	
}
Object.freeze(head_default);

//SETTING
export const setting_default = {
	background:  "hsla(50, 75%, 75%, 1)", //background (neutral color) //#efdf8f
	background_2: "hsla(44, 92%, 48%, 1)", //background of the whole page //#ebaf0a
	highlight: "hsla(10, 100%, 40%, 1)", //important, buttons, headlines //#c20

	text: "hsla(10, 90%, 20%, 1)", //default color of text //#611405
	enable_online: true, //online context (pictures downloaded from wikimedia commons)
	enable_maps: true, //jvector maps 
	units:  navigator.language === "en-US" ? "us" : "si", //preferable units cm x inches etc. 
	difficulty: 0, 
}
Object.freeze(setting_default);





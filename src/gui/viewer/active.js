import { writable, get } from 'svelte/store';
import cat from 'Questions';
import tags from 'Data/tags.json';

function teleport(category, level, question){
	const direction = (()=>{
		if(category !== undefined && category < get(active).category) return -1;
		if(level !== undefined && level < get(active).level) return -1;
		if(question !== undefined && question < get(active).question) return -1;
		return 1;
	})();
	
	category ??= 0;
	level ??= 0;
	question ??= 0;


	if(question >= cat[category]?.[level]?.length){
		level++;
		question = 0;
	}else if(question < 0){
		level--;
		if(cat[category]?.[level]?.length) question = cat[category][level].length; //if does not exist, the category gets overriden anyway
	}
	
	for(let fail = 20; fail; fail--){
		if(level > 2){
			category++;
			level = 0;
		}else if(level < 0){
			category--;
			level = 2;
		}
		
		if(category < 0) category = cat.length - 1;
		if(category >= cat.length) category = 0;

		if(cat[category][level].length){
			return {category, level, question}
		}else{
			level = level + direction;
		}
	}
	
	//TODO ERROR
}
	



function create_active(input){
	const { subscribe,  update  } = writable(input);

	return {
		subscribe,
		update,

		display: (what) => update(active => {
			active.display = what;
			return active;
		}),

		teleport: (category, level, question) => update(active => {
			category ??= active.category; 
			level ??= active.level; 
			question ??= active.question; 
			active = {
				...active,
				...teleport(category, level, question),
			}
			return active;
		}),

		move: (category = 0, level = 0, question = 0) => update(active => {
			active = {
				...active,
				...teleport(
					active.category + category, 
					active.level + level,
					active.question + question,
				),
			}
			return active;
		}),
		
		tag: (tag) => update(active => {
			active.tag = tag;
			return active;
		}),

	};
}











export const active = create_active({
	display: "table",
	category: 0,
	question: 0,
	level: 0,
	tag: "",
}); 




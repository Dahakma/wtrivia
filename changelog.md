# Changelog

### 0.9.7

- fixed *random mix* not re-initializing categories every round<br> 
- few minor improvements

### 0.9.6

- improved handling of questions, categories and tags <br>
- improved the code of the main game loop <br>
- state of the game is remembered after reload <br>
- minor visual improvements <br>

### 0.9.3

- fixed several typos, minor toggling with style <br>

### 0.9.2

- fixed bugged difficulty & survival mode <br>

### 0.9.0 

- working game <br>
- over 2800 questions  <br>
<!----
- TODO - user-adjusting tag weight <br>
- TODO - maps in active mode (accepting input) <br>
- TODO - happy and sad sounds <br>
---->

# How the Game Works

- in `vite.config.js` is the setting for bundling game
- `npm run dev` or `npm run start` will run the game in development mode
- `npm run build` will build the game for production and puts the whole finished game in `/dist/` folder
- the content of `/public/` gets moved into `/dist/` folder during bundling with paths to it unchanged (favicon and fonts)
- in folder `/questions/` are questions in the form of raw text files. They have to be manually compiled by running `npm run q` which turns them into JSON located in `/src/data/`
- `/scripts/` contains custom scripts, like the one compiling the text files with questions into JSON
- `/src/` contains the actual game
- `/src/libraries/` are libraries handling auxiliary stuff like colours or seeded randomness
-  default variables and constants are saved in `/src/system/variables.js` 
- `/src/system/setting.js` will create the store `setting` with global game settings (saved in *localStorage* as `wtrivia_setting`)
- `/src/system/head.js` will create store the `head` controlling the main game loop and containing settings related to the current game, available categories and questions, score and the currently displayed screen (saved in *localStorage* as `wtrivia_head`)
- `/src/gui/` contains svelte components with different "screens". `/src/gui/Page.svelte` switches between them based on  `head`
- `/changelog.md` gets automatically added to the game


[🔙 Back to Index](index.md)
const input = './questions/'; //input folder (path from (I assume) package.json)
const input_js =  '../questions/';  //path to input folder from this script (dunno, but it works)
const output =  './src/data/';


import * as fs from 'fs';


/*
const input = './questions/questions/'; //input folder with questions
const output = './src/data/questions.json';  //output file with database of questions
const output_tags = './src/data/tags.json'; //output file with tags 



//import {tags} from '../questions/tags.js';
const path =  '../questions/tags.js';
const {tags} = await import(`path)


const allowed_tags = Object.keys(tags);

import {list as newest_categories} from '../questions/newest.js';



console.log(allowed_tags);
*/


//VARIABLES 
let errors = 0;
let warnings = 0;
const cats = [];
let allowed_tags = [];
let newest_categories = [];

//LOGGING 
const a = {
	reset: "\u001b[0m",
	yellow: "\u001b[33;1m",
	green: "\u001b[32;1m",
	blue: "\u001b[36;1m",
	red: "\u001b[31;1m",
	white: "\u001b[37;1m",
	grey: "\u001b[37m",
}

const log = {
	blue(input){
		console.log(`${a.blue}${input}${a.reset}`);
	},
	yellow(input){
		warnings++;
		console.log(`${a.yellow}${input}${a.reset}`);
	},
	grey(input){
		console.log(`${a.grey}${input}${a.reset}`);
	},
	red(input){
		errors++;
		console.log(`${a.red}${input}${a.reset}`);
	},
	green(input){
		console.log(`${a.green}${input}${a.reset}`);
	},
	white(input){
		console.log(`${a.white}${input}${a.reset}`);
	},
	head(input){
		this.white(``);
		this.white("***************************");
		this.blue(`*** Question database "${input}" ***`);
		this.white(``);
	},
	done(input){
		this.white("***************************");
		this.green(`"${input}" compiled!  `);
		if(errors) this.red(`${errors} ${errors === 1 ? "ERROR" : "ERRORS"}`);
		if(warnings) this.yellow(`${warnings} ${warnings === 1 ? "WARNINGS" : "WARNINGS"}`); 
		this.white("***************************");
	},
}




//GOING THROUHG INPUT FILES 
//fs.readdirSync(input).forEach(database => {  //in case there are multiple databases
let database = "wtrivia_default";
//let database = "test";

	log.head(database);
	
	errors = 0;
	warnings = 0;
	cats.length = 0;


	const {tags} = await import(`${input_js}${database}/tags.js`)
	allowed_tags = Object.keys(tags);
	const {list} = await import(`${input_js}${database}/newest.js`)
	newest_categories = list;



	process_folder(`${input}${database}/questions`, []);


database = "questions";

//console.log(cats);
	//writes everything in the output file 
	fs.writeFileSync(`${output}${database}.json`, JSON.stringify(cats, undefined, 2).replace(/'/g, "\u2019"), (err)=> {  //TODO - I don't remember what replace does
	  if (err) throw err;
	});

//${database}_ TODO!!!
	//tag file
	fs.writeFileSync(`${output}tags.json`, JSON.stringify(tags, undefined, 2).replace(/'/g, "\u2019"), (err)=> {
	  if (err) throw err;
	});



	log.done(database);
//});


function process_folder(original_path, folders){  //goes through folders, as deep as necessary
	const path = `${original_path}/${folders.join("/")}`;

	if( fs.lstatSync(path).isDirectory()  ){
		fs.readdirSync(path).forEach(folder => {
			process_folder(original_path, [...folders, folder]);
		});
	}else{
		process_file( fs.readFileSync(path, 'utf8'), folders); //TODO - check for broken file/wrong format
	}
}







//PROCESS FILE
function process_file(data, folders){

	//id 
	const id = folders.join("_").replace(`.txt`,``);
	log.white(id);

	//data 
	data = remove_comments(data);
	const lines = data.toString().split("\n").map( line => line.trim() );
	//turns lines into object with category or null
	const category = create_category(lines);
	if(category === null) return log.red(`"${id}" is corrupted and was skipped`);
	
	//name (newest)
	const name = (()=> {	
		//id of category included in file "newest_categories"
		if( newest_categories.includes(id) ){  
			log.blue(`	- tagged (*new*)`); 
			category.tags.push(`newest`);
			return `${category.name} (*new*)`;
		}else{
			return category.name;
		}
	})();

	//implicit tags 
	const implicit_tags = folders;
	implicit_tags.pop(); //the last folder is "name_of_file.txt"
	implicit_tags.forEach( tag => category.tags.push(tag) );
	category.tags = [...(new Set(category.tags))]; //remove duplicated tags

	//check tags 
	check_tags(category);

	//add
	cats.push({
		id,
		...category,
		name,
	})

}





function create_category(lines){
	if(lines === undefined || !Array.isArray(lines) || lines.length === 0) return null;

	const cat = {
		"0": [], //easy
		"1": [], //medium
		"2": [], //hard
		tags: [],
	}
	let level = -1; //0 - easy; 1 - medium; 2 hard 
	let question = undefined;
	
	
	for (const line of lines){
		if(line === "" && question === undefined) continue; //skips empty lines
	
		//HEAD
		if(level === -1){
			
			//NAME
			if( line.startsWith("NAME ") ){ //name explicitly 
				cat.name = line.replace("NAME", "").trim();
			}else if(!cat.name){ //implicitly the first text is the name of the category 
				cat.name = line; 
			
			//AUTHOR
			}else if( line.startsWith("AUTHOR") ){
				let temp = line.split(" ");
				temp.shift(); //removes the tag,
				cat.author = temp;  //array (multiple authors possible)

			//TAG or TAGS
			}else if( line.startsWith("TAG") ){
				let temp = line.split(" ");
				temp.shift(); //removes the tag
				temp = temp.filter( a => a ); //remove empty ""
				cat.tags = temp;  //array (multiple authors possible)

			
					//SHARED ONLY LEGACY !!!
					}else if( line.startsWith("SHARED_ONLY") ){
						cat.shared =  line.replace("SHARED_ONLY","").trim(); 

			//SHARED 
			}else if( line.startsWith("SHARED") ){
				cat.shared =  line.replace("SHARED","").trim(); 

					//ONLINE - TODO LEGACY 
					}else if( line.startsWith("ONLINE") ){
						cat.tags.push("online");
					
					//XMAP - TODO LEGACY 
					}else if( line.startsWith("XMAP") ){
						cat.tags.push("xmap");



			//EASY
			}else if( line.startsWith("EASY") ){ //starts easy questions
				level = 0;
				continue;

			//FINAL
			}else if( line.startsWith("FINAL") ){ //starts medium questions (final category- only one difficulty)
				level = 1;
				continue;

			//nothing
			}else{ //implicitly assumes questions started without proper tag and there is only one difficulty 
				log.yellow(`	- no initial tag (EASY or FINAL)`);
				level = 1;
			}
	
			if(level === -1) continue;
		}	



	//BLOCKS or LEVELS
		 if( line.startsWith("BLOCK") ){ //creates block of similar questions within a level
			create_block(cat, level);
			continue;
		
		}else if( line.startsWith("MEDIUM") ){ 
			finish_blocks(cat, level)
			if(level !== 0) log.yellow(`	- tag MEDIUM does not follow tag EASY`);
			level = 1;
			continue;

		}else if( line.startsWith("HARD") ){ 
			finish_blocks(cat, level);
			if(level !== 1) log.yellow(`	- tag HARD does not follow tag MEDIUM`);
			level = 2;
			continue;
		
		}else if( line.startsWith("EASY") ){  //EASY tag shoud definitely be here! Added here for script to work even if the order of question difficulties is really messed up
				finish_blocks(cat, level)
				log.yellow(`	- tag EASY should be first! `);
				level = 0;
				continue;

		}

		

	//QUESTION
		//EMPTY LINE - question finished 
		if(line === ""){
			let stillborn = true;
			if(question.que) stillborn = false; //question has text
			if(cat.shared) stillborn = false; //or all question share one
			if( cat.tags.includes("img") || cat.tags.includes("hd") ) stillborn = false; //or can use generic for images ("On the picture is")
			if( cat.tags.includes("map") || cat.tags.includes("xmap") ) stillborn = false; //or can use generic for images ("On the map is highlighted" or "Find on the map")
			if( stillborn ) log.yellow(`questions has no text (${level};${cat[level].length}) `);

			if( question.ans.length === 0 || !question.ans[0] ){ //TODO - question without answers? 
				log.yellow(`questions has no answer (${level};${cat[level].length}) `);
				stillborn = true;
			}

			if(!stillborn) cat[level].push({...question});
			question = undefined;
			continue;
		}

		// IMAGE or MAP
		function multimedia(tag){
			if(question === undefined){ //question could be without que when img or map
				question = {
					ans: [],
				}; 
			}
			let temp = line.replace(tag, "").trim();
			const key = tag.toLowerCase();
			if(tag === "IMG" || tag === "HD"){
				question[key] = trim_image(temp);
			}else if(tag === "MAP" || tag === "MARK"){ //TODO - xmap is legacy; remove 
				temp = temp.split("MARK");
				if(tag === "MAP") question.map = temp.shift().replaceAll(" ","");
				while(temp.length){
					const mark = trim_mark( temp.shift() );
					if(mark !== undefined){
						if(question.marks === undefined) question.marks = [];
						question.marks.push(mark);
					}
				}
			}else{
				question[key] = temp;
			}
		}	

		const tag = line.split(" ")[0]; //fist word on the line
		if( ["IMG", "HD", "MAP", "MARK", "XMAP", "EXPY"].includes(tag) ){
			multimedia(tag);
			continue;
		}
		
		
		//QUESTION
		if(question === undefined){
			question = {
				que: line,
				ans: [],
			};
			continue;
		}

		//ANSWER
		if(question){
			question.ans.push(line);
			continue;
		}

		
	}

	finish_blocks(cat, level);
	return cat;
}



//CHECK TAGS - detects possible issues with tags 
function check_tags(category){
	if( !category?.tags?.length ) return log.yellow(`	- category has no tags`);
	category.tags.forEach( tag => {
		if( !allowed_tags.includes(tag) ){
			log.yellow(`	- non-standard tag "${tag}" `);
		}
	})

	const all = [...category[0], ...category[1], ...category[2]];
	//if( all.every( a => a.xmap ) && !category.tags.includes("xmap") ) log.yellow(`	- all questions use XMAP but the category has no tag "xmap" `);
	if( all.every( a => a.map ) && !category.tags.includes("map")  ) log.yellow(`	- all questions use MAP but the category has no tag "map" `);
	if( all.every( a => a.img ) && !category.tags.includes("images") ) log.yellow(`	- all questions use IMG but the category has no tag "images" `);
	log.grey(`	- ${category.tags.join("; ")}`);
}



//BLOCKS
//levels (easy/medium/hard) of questions could be futher divided into blocks 
function create_block(cat, level){
	const index = cat[level].length - 1;
	if(cat.block === undefined) cat.block = []; //no blocks - initiates
	if(cat.block[level] === undefined){ //no blocks on this level - everything till now is set as the first block
		cat.block[level] = [];
		cat.block[level].push( [0, index] );
	}else{ //blocks on this level - creates a new block, from the end of the previos one till now
		const last_index = cat.block[level].at(- 1)[1]; //const last_index = cat.block[level][ cat.block[level].length - 1  ][1];
		cat.block[level].push( [last_index + 1, index] );
	}
}

//called when the level is finished; to close an unclosed blocks if there are some
function finish_blocks(cat, level){
	if(cat.block && cat.block[level]){
		create_block(cat, level) ;
	}
}



//COMMENT 
//txt files can contain javascript-like comments
function remove_comments(data){
	data = data.replace(/(?<!https:)\/\/(.*)/gm, ``); // removes everything in line after // ; ignoring hyperlinks
	data = data.replace(/\/\*(.|\s)*?\*\//gm, ``); // removes everything between /* */
	return data;
}


//TRIM IMAGE - TODO 
function trim_image(adress){
	adress = adress.replace("https://upload.wikimedia.org/wikipedia/commons/thumb", "§WCT"); //TODO thumb removed
	return adress;
}

//TRIM MAP
function trim_mark(input){ //changes "it;41°53′N 12°30′E" to "it;41.8833;12.5"

	const output = parse_geodata(input);
	if(!output || output[0] === undefined || output[1] === undefined) return undefined; //TODO - THIS OR IS THIS STUPID? 
	return output;

/*
		if(!input.match(/°/)){ 

		//
			(-?\d+\.?\d*)
				\s*[,;]*\s*
			(-?\d+\.?\d*)
		//
	
		const [useless, latitude, longitude] = /(-?\d+\.?\d*)\s*[,;]*\s*(-?\d+\.?\d*)/.exec(input); 
		return [latitude, longitude];
	}
	

	
	const coordinates = input.trim().replace(/,| |;/g, ""); //doesnt care if divided by "," or " " or ";"
	//
		/
			(\d+\.?\d*)°
			(\d+)?[′|']?
			(\d+\.?\d*)?[″|”]?
			(\w)
			
			\s*[,;]*\s*

			(\d+\.?\d*)°
			(\d+)?[′|']?
			(\d+\.?\d*)?[″|”]?
			(\w) 
		/
	//
	const code = /(\d+\.?\d*)°(\d+)?[′|']?(\d+\.?\d*)?[″|”]?(\w)\s*[,;]*\s*(\d+\.?\d*)°(\d+)?[′|']?(\d+\.?\d*)?[″|”]?(\w)/.exec(coordinates); 


	if(code === null) return undefined; //broken

	const [uselesss, y_hour, y_minute, y_second, N, x_hour, x_minute, x_second, E] = code.map(a => {
		if(!a) return 0; //undefined
		if( isNaN(a) ) return a; //"N" or "S" or "E" or "W"
		return +a; //number in string format
	}); 
	
	let yyy = y_hour + (y_minute * 1/60) + (y_second * 1/3600);
	let xxx = x_hour + (x_minute * 1/60) + (x_second * 1/3600);
	
	if(N !== "N") yyy *= -1; //southern hemisphere
	if(E !== "E") xxx *= -1; //western hemisphere
	
	yyy = Math.round( yyy * 10000 ) / 10000; //rounding to 0.0001 gives accuracy ~10 m
	xxx = Math.round( xxx * 10000 ) / 10000;
	

	return [yyy, xxx];
*/
}














/**
 * turns string with coordinates and returns [latitude, longitude] or [undefined, undefined]
 * @param {string} input
 * @returns {Array} coordinates
 * @returns {number} coordinates[0] - latitude (x)
 * @returns {number} coordinates[1] - longitude (y)
 */
function parse_geodata(input){

	if( input.match("N") || input.match("S") ){ 

		const coordinates = input.trim().replace(/,| |;/g, ""); //doesnt care if divided by "," or " " or ";"
		/*
			/
				(\d+\.?\d*)°
				(\d+)?[′|']?
				(\d+\.?\d*)?[″|”]?
				(\w)
				
				\s*[,;]*\s*

				(\d+\.?\d*)°
				(\d+)?[′|']?
				(\d+\.?\d*)?[″|”]?
				(\w) 
			/
		*/
		const code = /(\d+\.?\d*)°(\d+)?[′|']?(\d+\.?\d*)?[″|”]?(\w)\s*[,;]*\s*(\d+\.?\d*)°(\d+)?[′|']?(\d+\.?\d*)?[″|”]?(\w)/.exec(coordinates); 

		if(code === null) return [undefined, undefined]; //broken

		const [uselesss, y_hour, y_minute, y_second, N, x_hour, x_minute, x_second, E] = code.map(a => {
			if(!a) return 0; //undefined
			if( isNaN(a) ) return a; //"N" or "S" or "E" or "W"
			return +a; //number in string format
		}); 
		
		let yyy = y_hour + (y_minute * 1/60) + (y_second * 1/3600);
		let xxx = x_hour + (x_minute * 1/60) + (x_second * 1/3600);
		
		if(N !== "N") yyy *= -1; //southern hemisphere
		if(E !== "E") xxx *= -1; //western hemisphere
		
		yyy = Math.round( yyy * 10000 ) / 10000; //rounding to 0.0001 gives accuracy ~10 m
		xxx = Math.round( xxx * 10000 ) / 10000;
		

		return [yyy, xxx];

	}else{

		/*
			(-?\d+\.?\d*)\s*°?
				\s*[,;]*\s*
			(-?\d+\.?\d*)\s*°?
		*/
	

		const code = /(-?\d+\.?\d*)\s*°?\s*[,;]*\s*(-?\d+\.?\d*)\s*°?/.exec(input); 
		if(code === null) return [undefined, undefined]; //broken
		const latitude = +code[1];
		const longitude = +code[2];

		return [
			isNaN(latitude) ? undefined : latitude,
			isNaN(longitude) ? undefined : longitude,
		];
	}
	
}
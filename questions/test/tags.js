export const tags = {

//MASTER TAGS
	newest: {
		name: "newest",
		desc: `recently addded category`,
	},

	todo: {
		desc: `work in progress`,
	},

	images: {
		name: `Recognise Image 📷`,
		desc: `involves images`,
		suffix: `📷`,
	},

	xmap: {
		name: `Find on Map  🌐`,
		desc: `clicking on interactive maps`,
		suffix: `🌐`,
	},

	map: {
		name: `Recognise on map 🌎`,
		desc: `involves maps`,
		suffix: `🌎`,
	},

	serious: {
		desc: `recent events & heavy topics`,
	},
	
//SYSTEM
	hd: {
		name: `Bigger images`,
		desc: `Higher-definition images available`,
		hidden: true,
	},
	
	
//MAJOR 
	tutorial: {
		name: `Quick How To`,
		desc: `Related to *Quick How To* guides to make sure they actually work`,
		hidden: true,
	},
	
};
map format

SHARED On the map is 

MAP ceu
Central Europe

MAP ceu;DE
Germany highlighted

MAP ceu; DE
Germany highlighted (space)

MAP ceu;DE+
Germany only zoomed 

MAP ceu MARK 52°31′12″N 13°24′18″E
Central Europe - Berlin (oneline)

MAP ceu;DE MARK 52°31′12″N 13°24′18″E
Germany highlighted - Berlin (oneline)

MAP ceu;DE+ MARK 52°31′12″N 13°24′18″E
Germany only zoomed - Berlin (oneline)

MAP ceu 
MARK 52°31′12″N 13°24′18″E
Central Europe - Berlin (multiline)

MAP ceu;DE 
MARK 52°31′12″N 13°24′18″E
Germany highlighted - Berlin (multiline)

MAP ceu;DE+
MARK 52°31′12″N 13°24′18″E
Germany only zoomed - Berlin (multiline)

MAP ceu 
MARK 52°31′12″N 13°24′18″E
MARK 53°33′N 10°00′E
Central Europe - Berlin, Hamburg (multiline)

MAP ceu;DE 
MARK 52°31′12″N 13°24′18″E
MARK 53°33′N 10°00′E
Germany highlighted - Berlin, Hamburg (multiline)

MAP ceu;DE+
MARK 52°31′12″N 13°24′18″E
MARK 53°33′N 10°00′E
Germany only zoomed - Berlin, Hamburg (multiline)

MAP ceu MARK 52°31′12″N 13°24′18″E MARK 53°33′N 10°00′E
Central Europe - Berlin, Hamburg (oneline)

MAP ceu;DE MARK 52°31′12″N 13°24′18″E MARK 53°33′N 10°00′E
Germany highlighted - Berlin, Hamburg (oneline)

MAP ceu;DE+ MARK 52°31′12″N 13°24′18″E MARK 53°33′N 10°00′E
Germany only zoomed - Berlin, Hamburg (oneline)

MAP ceu 
MARK 52°31′12″N 13°24′18″E MARK 53°33′N 10°00′E
Central Europe - Berlin, Hamburg (mark multiline)

MAP ceu;DE 
MARK 52°31′12″N 13°24′18″E MARK 53°33′N 10°00′E
Germany highlighted - Berlin, Hamburg (mark multiline)

MAP ceu;DE+
MARK 52°31′12″N 13°24′18″E MARK 53°33′N 10°00′E
Germany only zoomed - Berlin, Hamburg (mark multiline)
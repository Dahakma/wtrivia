Space Conquest

TAGS history technology space science

EASY
The first artificial satellite *Sputnik 1* was launched by Soviets on
4 October 1957
3 September 1953
5 November 1961
6 December 1965

The main American spaceport Cape Canaveral is located in
Florida
Texas
California
North Carolina
 
The first American in space (May 5, 1961) was 
Alan Shepard
John Glenn
Gus Grissom
Neil Armstrong

The first human on the Moon was 
Neil Armstrong
Alan Shepard
John Glenn
Gus Grissom

The formerly Soviet Baikonur Cosmodrome is now located in 
Kazakhstan
Belarus
Azerbaijan
Ukraine

MEDIUM
The first space station was
Salyut I
Mir
ISS
Spacelab

The *second* human on the Moon was 
Buzz Aldrin
Michael Collins 
Pete Conrad 
Jim Lovell

The ship of Yuri Gagarin was named 
Vostok 1 *(East)*
Soyuz 1 *(Union)*
Kosmos 1 *(Cosmos)*
Salyut 1 *(Salute)*

The first satellite sent into space by the USA on 31 January 1958 {4 months after Sputnik} was
Explorer 1
Voyager 1
Progress 1
Vanguard 1

The *proper* term for *spacewalk* is shortened by letters 
EVA
TMG 
LES
HUT

The first female astronaut became in 1963
Valentina Tereshkova
Svetlana Savitskaya
Sally Ride
Judith Resnik

*Shenzhou 5*, with the first Chinese astronaut Yang Liwei, was launched
§5§2003§<2019§@y

The name of rockets used in the Apollo space program was
Saturn
Jupiter
Neptune
Mercury

The first object crossing the *Kármán line* (100 kilometres above Earth's mean sea level), regarded as the boundary between our atmosphere and outer space, was launched 1944 by
Germans
Soviets
Americans
Brits 

HARD
Soviets probes Venera explored
Venus
Mars
Mercury 
Moon

The leading Soviet rocket engineer and spacecraft designer during the Space Race was
Sergei Korolev
Mitrofan Nedelin
Igor Komarov
Konstantin Tsiolkovsky

The first manned spaceflight around the Moon and back was
Apollo 8
Apollo 2
Apollo 10
Apollo 5

The first successful landing of a space probe on Venus was accomplished by 
Soviets
Americans
Chinese
Japanese 

The first (and so far only) attempt to reach and explore Pluto was the spacecraft 
New Horizons
Cassini–Huygens
Voyager 2
Rosetta

The first non-American non-Soviet astronaut was from
Czechoslovakia
France
Poland
United Kingdom

Dong Fang Hong I,*The East is Red*, the first Chinese satellite was launched
§10§1970§<2000§@y

The first satellite constructed by other country than USA or USSR but launched on American rocket in 1962 was made in
IMG https://upload.wikimedia.org/wikipedia/commons/e/e5/Alouette_1.jpg
Canada
France
Japan
Italy

The third country which was able to launch a satellite on its own rocket was in 1965
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Asterix_Musee_du_Bourget_P1020341.JPG/614px-Asterix_Musee_du_Bourget_P1020341.JPG
France
United Kingdom
Japan
China

How many people walked on the Moon?
12
10
8
6
Mountains

TAGS geography sport

EASY
The longest mountain range is 
Andes Mountains
Himalayan Mountains
Rocky Mountains
Carpathian Mountains

Atlas Mountains are locate in 
North Africa
Europe
Middle East
Central Asia

The highest African mountain is
Mount Kilimanjaro 
Mount Kenya
Mount Stanley 
Mount Karisimbi

The second highest mountain in the world {8614@m} is 
K2
Lhotse
Annapurna
Shishapangma

The highest mountain of North America - 20310@ft - is
Denali (Mount McKinley)
Mount Logan
Pico de Orizaba (Citlaltépetl)
Popocatépetl

MEDIUM
The highest mountain of Australia - 2228@m - is 
Mount Kosciuszko
Bimberi Peak
Mount Townsend
Ayers Rock 

Mount Everest, the highest mountain on the Earth, is high
§300§8848§@m

Mount Everest is shared between two countries. Across its summit point runs the international border between
Nepal and China
India and China
India and Bhutan
Nepal and India

The Matterhorn, one of the highest summits in Alps and Europe, straddles border between Switzerland and
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Matterhorn_from_Domh%C3%BCtte_-_2.jpg/640px-Matterhorn_from_Domh%C3%BCtte_-_2.jpg
HD https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Matterhorn_from_Domh%C3%BCtte_-_2.jpg/1080px-Matterhorn_from_Domh%C3%BCtte_-_2.jpg
Italy
France
Liechtenstein
Germany
Austria

The highest mountain of Africa, Kilimanjaro, is located in 
Tanzania
Uganda
Kenya
Ethiopia

The Drakensberg is a mountain range located in 
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/South_Africa_-_Drakensberg_(16261357780).jpg/640px-South_Africa_-_Drakensberg_(16261357780).jpg
South Africa 
Angola
Cameroon
Democratic Republic of the Congo

The highest mountain located outside of Asia is the highest mountain of 
South America
Africa
North America
Europe

The Third highest mountain in the world - 8586@m - is
Kangchenjunga 
Shishapangma
Nanga Parbat
Makalu


HARD
The highest mountain of South America - 6961@m - is
Aconcagua
Ojos del Salado
Monte Pissis
Huascarán

All eight-thousanders are located in two mountain ranges - Himalay and 
Karakoram
Hindu Kush
Andes 
Altai

The highest mountain of Africa, Kilimanjaro, is high
§900§5895§<9000§>2000§@m

The Rwenzori Mountains are shared by 
Uganda and Democratic Republic of the Congo
Tanzania and Ethiopia
Kenya and Rwanda
Burundi and Central African Republic

Mount Tahat is the highest peek of the Hoggar Mountains. It could be found in 
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Tahat_matin.jpg/640px-Tahat_matin.jpg
HD https://upload.wikimedia.org/wikipedia/commons/a/a1/Tahat_matin.jpg
North Africa
Middle East
Central Asia
Southeast Asia

How many Eight-thousanders there are?
§6§14

The deadliest Eight-thousander with successful summits:deaths ration 34% {191:61} is 
Annapurna
Nanga Parbat
Mount Everest
Kangchenjunga 

The first peson who climbed all eight-thousanders (without supplementary oxygen) became in 1986
Reinhold Messner (Italian)
Jerzy Kukuczka (Polish)
Erhard Loretan (Swiss)
Carlos Carsolio (Mexican)

The highest active volcano in the world {6893@m} is
Ojos del Salado (Argentina/Chile)
Nevado Sajama (Bolivia)
Coropuna (Peru)
Chimborazo (Ecuador)

The highest mountain of Oceania {4884@m} is
Puncak Jaya (Papua, Indonesia)
Mauna Kea (Hawaii; USA)
Mont Puke (Wallis and Futuna; France)
Aoraki / Mount Cook (New Zealand)

The highest mountain in Antarctica {4892@m} is
Vinson Massif
Mount Erebus
Hawkes Heights
Mount Siple

/*



# Mountains

# Eight-thousanders 

is an exclusive club of 14 mountains more than 8 000 metres above the sea level. All are located in Asian in the Himalayan and Karakoram mountain ranges.

https://en.wikipedia.org/wiki/Eight-thousander#/media/File:Comparison_of_highest_mountains.svg

list

# Seven Summits

are the highest mountains of each of the seven traditional continents

- Mount Elbrus is located in Greater Caucasus, if we do not consider Caucaus part of the Europe, the highes mountain is Mont Blanc in Alps
- Puncak Jaya is located on New Guinea; the highest mountain of mainlan Australia is Mount Kosciuszko
- Hawaiian Mauna Kea would be the highest mountain of tectonic Pacific plate




Self-sufficient pure climbing without oxygen, fixed ropes or porters beyond the base camp is known as 
Alpine style
Himalayan style
Expedition style
Adventurous style


The highest mountain in the Alps and Western Europe is
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Mont_Blanc_from_Aosta_Valley.JPG/640px-Mont_Blanc_from_Aosta_Valley.JPG
HD https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Mont_Blanc_from_Aosta_Valley.JPG/1277px-Mont_Blanc_from_Aosta_Valley.JPG
Mont Blanc
Dufourspitze
Matterhorn
Grossglockner 
Zugspitze


In 2019 was the Mount Everest summited by 
§350§891§<1400§>100§people


The highest permanent settlement, ihabited by over 30_000 people, is *La Rinconada*. It is located 5100@m high above see level, far above the tree line with average temperature 1.3@c. The towng exists because of gold, mostly mined in unregulated *artisanal* mines. Many miners work under *cachorreo* system when they work for 30 days without payment and for one day they are allowed to work for themselves (plus with occassional pocketing of promising chunks). *La Rinconada* lies in
Peru
Mexico
Brazil
Argentina
Colombia

The capital city with the highest elevation, 3640@m above the sea level is 
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/La_Paz_01.jpg/487px-La_Paz_01.jpg
HD https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/La_Paz_01.jpg/730px-La_Paz_01.jpg
La Paz, Bolivia
Mexico City, Mexico
Kathmandu, Nepal
Addis Ababa, Ethiopia
Kabul, Afghanistan
Bern, Switzerland



The highest country by average elevation - 3280@m - is 
Bhutan
Andorra
Chile
Rwanda
Armenia


The highest, the second highest, the third... and the tenth highest mountains in the USA are located in Alaska. The eleventh and the highest mountain in contiguous United States is 
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Mount_Whitney_2003-03-25.jpg/640px-Mount_Whitney_2003-03-25.jpg
HD https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Mount_Whitney_2003-03-25.jpg/1024px-Mount_Whitney_2003-03-25.jpg
Mount Whitney, California
Mount Elbert, Colorado
Mount Massive, Colorado
Mount Rainier, Washington


K2 is the second highest mountain in the world - 8611@m above sea level. It is infamous for being difficult to climb and deadly. It was first summited by Italians Achille Compagnoni and Lino Lacedelli in 1954, during winter it was summited for the first time by Nepali expedition in 2021. K2 was originaly a notation used by trigonometrical survey (*K* standing for *Karakoram*) but while local names were used for other mountains, K2 is so remote it is not even visible from the nearest habitations. The peak of K2 is located in territory controled by
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Chogori.jpg/640px-Chogori.jpg
HD https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Chogori.jpg/960px-Chogori.jpg
Pakistan
India
Nepal
China
Bhutan


denali
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Wonder_Lake_and_Denali.jpg/623px-Wonder_Lake_and_Denali.jpg
HD https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Wonder_Lake_and_Denali.jpg/934px-Wonder_Lake_and_Denali.jpg








*/
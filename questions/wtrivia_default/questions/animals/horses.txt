Horses

TAGS animals hd

EASY
The offspring of a male donkey and a female horse is 
Mule
Hinny
Onager
Pony

A female horse under the age of four is 
Filly
Mare
Colt
Hinny

A male horse under the age of four is 
Colt
Stallion
Gelding
Filly

Which of following sports includes horses? 
Modern pentathlon
Triathlon
Field hockey
Korfball

Sort horse gaits from the slowest to the fastest
§<§Trot;Canter;Gallop

Likely the most famous horse of antiquity was Bucephalus. He carried his master into many battles and charges and when he died a city was named after him. He was the horse of 
Alexander the Great
Julius Caesar
Hannibal Barca
Pyrrhus of Epirus

MEDIUM
The offspring of a male horse and a female donkey is
Hinny
Mule
Pony
Foal

Incitatus made a quite career. According to the legend, his owner, the Roman emperor, planned to make him a consul. The crazy/joking/slandered emperor was 
Caligula
Nero
Commodus
Tiberius

The most famous steeplechase in the world is the Grand National {4 miles and 2½ furlongs long, 30 fences over two laps and prize fund of £1 million} run annually at 
Aintree Racecourse, Liverpool
Windsor Racecourse, Winsdor
Alexandra Park Racecourse, London
Goodwood Racecourse, Chichester

*Lipizzan* horses (originating in Slovenia and developed with the support of the Habsburg nobility) are notehworty for their colour. They are 
White 
Black
Chestnut
Dun

As one of the greates racing horses in the history - who in 1973 won Triple Crown (races Kentucky Derby, Preakness Stakes, and Belmont Stakes) while setting still-standing speed records in all three - is regarded
Secretariat
Seabiscuit
Skip Away
Sixteen


HARD
The *Horsepower* is a unit of measurement of power. It was promoted by James Watt as a way to market his steam engine. Mechanical (imperial) / metric horsepowers are
745.7 and 735.5 watts
330.9 and 326.3 watts
1221.0588 and 1204.2 watts
2135.8 and 2106.4 watts

One of the most difficult European steeplechaseses and the even with the longest sport tradition in the Czech Republic {since 1874} is 
Velká pardubická
Velká budějovická
Velká plzeňská
Velká liberecká

The only remaining specie of wild horse is *Przewalski's horse*. In the wild the specie went exctint and survived only in the captivity. In 1950 the worldwide population dropped under 30 indviduals. Thanks to conservation efforts (especially of the Prague ZOO) it was saved. The current population is around 2000 horses and it was even reintroduced to its natural habitatat in 
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/Przewalskis_horse_02.jpg/487px-Przewalskis_horse_02.jpg
HD https://upload.wikimedia.org/wikipedia/commons/f/f5/Przewalskis_horse_02.jpg
Mongolia
Ukraine
Botswana
Ethiopia

The *Percheron* is a draft breed originating from
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Pleasant_Hill_Percherons.jpg/640px-Pleasant_Hill_Percherons.jpg
HD https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Pleasant_Hill_Percherons.jpg/1082px-Pleasant_Hill_Percherons.jpg
France
England
Spain
Russia

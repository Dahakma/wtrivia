German and Austrian Federal States

TAGS ceu europe map xmap borders germany
SHARED On the map is highlighted

EASY
MAP de;DE-BY
Bavaria (Bayern)

MAP de;DE-BE
Berlin

MAP de;DE-BB
Brandenburg

MAP de;DE-NW
North Rhine-Westphalia (Nordrhein-Westfalen)

MAP at;AT-9
Vienna (Wien)

MAP at;AT-4
Upper Austria (Oberösterreich)

MAP at;AT-3
Lower Austria (Niederösterreich)

MEDIUM
MAP de;DE-NI
Lower Saxony (Niedersachsen)

MAP de;DE-HH
Hamburg

MAP de;DE-BW
Baden-Württemberg

MAP de;DE-SH
Schleswig-Holstein

MAP de;DE-HB
Bremen

MAP de;DE-SL
Saarland

MAP at;AT-7
Tyrol (Tirol)

MAP at;AT-2
Carinthia (Kärnten)

HARD
MAP de;DE-ST
Saxony-Anhalt (Sachsen-Anhalt)

MAP de;DE-RP
Rhineland-Palatinate (Rheinland-Pfalz)

MAP de;DE-MV
Mecklenburg-Vorpommern

MAP de;DE-TH
Thuringia (Thüringen)

MAP de;DE-SN
Saxony (Sachsen)

MAP de;DE-HE
Hesse (Hessen)

MAP at;AT-5
Salzburg

MAP at;AT-8
Vorarlberg

MAP at;AT-6
Styria (Steiermark)

MAP at;AT-1
Burgenland
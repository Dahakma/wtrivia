Leaves 

TAGS images nature
SHARED In the picture are leaves of 

EASY
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Cherry-branch-01.jpg/640px-Cherry-branch-01.jpg
Cherry

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Oak_leaves_(10493567083).jpg/640px-Oak_leaves_(10493567083).jpg
Oak

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Acer_pseudoplatanus_002.jpg/640px-Acer_pseudoplatanus_002.jpg
Maple

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Backlit_chestnut_leaves_(265937337).jpg/512px-Backlit_chestnut_leaves_(265937337).jpg
Chestnut

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Silver_birch_leaves.jpg/617px-Silver_birch_leaves.jpg
Birch

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Ginkgo_biloba_-_leaves.jpg/640px-Ginkgo_biloba_-_leaves.jpg
Ginkgo biloba

MEDIUM
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Beech_leaves.jpg/640px-Beech_leaves.jpg
Beech

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Apple_tree_leaf.jpg/337px-Apple_tree_leaf.jpg
Apple

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/EurAshLeaf.jpg/280px-EurAshLeaf.jpg
Ash

IMG https://upload.wikimedia.org/wikipedia/commons/6/65/TurkHazel.jpg
Hazel

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/American_Sycamore_Leaves_-_Flickr_-_treegrow.jpg/640px-American_Sycamore_Leaves_-_Flickr_-_treegrow.jpg
Sycamore

IMG https://upload.wikimedia.org/wikipedia/commons/7/75/Juglans-regia.JPG
Walnut

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/Weeping_Willow_leaf_(3212275000).jpg/640px-Weeping_Willow_leaf_(3212275000).jpg
Willow

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Holly_Leaves.JPG/640px-Holly_Leaves.JPG
Holly

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/Tilia_cordata_-_leaves_02.jpg/629px-Tilia_cordata_-_leaves_02.jpg
Lime/Linden

HARD
IMG https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Yellow_green_birch_leaves.jpg/640px-Yellow_green_birch_leaves.jpg
Birch

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/7/76/Ulmus_glabra.jpg/381px-Ulmus_glabra.jpg
Elm

IMG https://upload.wikimedia.org/wikipedia/commons/2/21/Beaked-Hazel-leaves-b_(6264229973).gif
Hazel

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Butte_Aspen_Yellow_Leaves_(3630772012).jpg/640px-Butte_Aspen_Yellow_Leaves_(3630772012).jpg
Aspen

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/PearBlossomsCalifornia.jpg/640px-PearBlossomsCalifornia.jpg
Pear

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Sambucus_nigra%2C_elder%2C_elderberry%2C_black_elder%2C_European_elder._5.jpg/360px-Sambucus_nigra%2C_elder%2C_elderberry%2C_black_elder%2C_European_elder._5.jpg
Elder

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/KudzuLeaves.JPG/640px-KudzuLeaves.JPG
Kudzu

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Fig_fruit.jpg/640px-Fig_fruit.jpg
Fig

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Starr-071024-0195-Laurus_nobilis-leaves-Enchanting_Floral_Gardens_of_Kula-Maui_(24867859296).jpg/360px-Starr-071024-0195-Laurus_nobilis-leaves-Enchanting_Floral_Gardens_of_Kula-Maui_(24867859296).jpg
Bay laurel

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Carica_papaya11.jpg/640px-Carica_papaya11.jpg
Papaya

IMG https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/Sassafras_tzumu_kz2.jpg/320px-Sassafras_tzumu_kz2.jpg
Sassafras
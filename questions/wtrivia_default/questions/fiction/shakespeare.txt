Bard of Avon

TAGS uk fiction art

EASY
To be, or not to be *(that is the question: Whether tis nobler in the mind to suffer the slings and arrows of outrageous fortune; Or to take Arms against a Sea of troubles; And by opposing end them: to die, to sleep)* is a famous quote from a play
Hamlet
Othello
Romeo and Juliet
King Lear

William Shakespeare was born and brought up in
Stratford-upon-Avon
London
Cambridge
Paris

Romeo and Juliet is
Tragedy
Comedy
Historical play
Sonnet

Iago is the villain of
Othello
Hamlet
King Lear
Macbeth

Shakespeare lived during the reign of
Elisabeth I
Henry VIII
Charles I
George I

Contemporary of Shakespeare was
Christopher Marlowe
Charles Dickens
Victor Hugo
Thomas More

Three Witches are characters from
Macbeth
The Tempest
Hamlet
The Winter's Tale

MEDIUM
*The Scottish Play* is an euphemism for the supposedly cursed play. A superstitious actor would never say aloud the title
Macbeth
Richard III
Titus Andronicus
Coriolanus

A girl, who tragically fell in love with Hamlet was
Ophelia
Gertrude
Juliet
Desdemona

Mercutio, one of the main characters of the play Romeo and Juliet is killed by
Tybalt
Romeo
Paris
Hamlet

Which of plays does NOT take place in Italy?
A Midsummer Nights Dream
The Taming of the Shrew
Much Ado About Nothing
The Merchant of Venice

Persuaded by Benvolio and Mercutio, Romeo attends the ball at the Capulet house in hopes of meeting a girl he has fallen in love with -
Rosaline, Juliets cousin
Juliet Capulet
Ness, Juliets handmaiden
Lady Capulet, Juliets mother

*Romeo and Juliet* takes place in
Verona
Rome
Venice
Florence

HARD
The St Crispins Day Speech *(We few, we happy few, we band of brothers)* is the highlight of the play
Henry V
Richard III
Julius Caesar
Macbeth

Benedick and Beatrice are the main characters of
Much Ado About Nothing
A Midsummer Nights Dream
The Merchant of Venice
The Taming of the Shrew

William Shakespeare lived between years
1564 - 1616
1541 - 1602
1518 - 1581
1587 - 1642

Shakespeare did not write a play about
Henry VII
Henry IV
Henry V
Henry VIII

The bloodiest and most violent work of Shakespeare (including 14 killings, 6 severed members, 1 rape, 1 live burial, 1 case of insanity and 1 of cannibalism) is
Titus Andronicus
Macbeth
Timon of Athens
The Merry Wives of Windsor

The play anachronistically featuring a clock before they were invented is
Julius Caesar
Antony and Cleopatra
Titus Andronicus
King Lear

After characters from the works of William Shakespeare are named moons of
Uranus
Neptune
Saturn
Jupiter
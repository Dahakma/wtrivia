export const tags = {

//MASTER TAGS
	newest: {
		name: "newest",
		desc: `recently addded category`,
		hidden: true,
	},

	todo: {
		desc: `work in progress`,
		hidden: true,
	},

	images: {
		name: `Recognise Image 📷`,
		desc: `involves images`,
		suffix: `📷`,
	},

	xmap: {
		name: `Find on Map  🌐`,
		desc: `clicking on interactive maps`,
		suffix: `🌐`,
hidden: true, //TODO
	},

	map: {
		name: `Recognise on map 🌎`,
		desc: `involves maps`,
		suffix: `🌎`,
	},

	serious: {
		desc: `recent events & heavy topics`,
	},
	
//SYSTEM
	hd: {
		name: `Bigger images`,
		desc: `Higher-definition images available`,
		hidden: true,
	},
	
	
//MAJOR
	animals: {
	},

	art: {
		desc: "high art, paintings, classic literature",
	},

	country: {
		name: "Country facts",
		desc: "details related to one country (or small set of countries)",
	},

	cities: {
		desc: "capitals, location on map; famous landmarks",
	},
	
	geography: {
		desc: "geographic features (no maps)",
	},

	fiction: {
		desc: "fictional works; books, movies and series",
	},

	flags: {
		desc: "recognise flag",
	}, 

	food: {
		desc: "food & beverages",
	},

	history: {
	},

	poi: {
		name: "Points of Interest", 
		desc: "maps with interesting places",
	},

	borders: {
		desc: "maps with states",
	},

	math: {
		desc: "numbers, calculations, geometry, units",
	},

	milestones: {
		desc: "important dates",
	},
	
	myth: {
		desc: "mythology",
	},
	
	science: {
	},

	scifi: {
		name: "science-fiction",
	},
	
	space: {
		desc: "astronomy & space exploration (no sci-fi)",
	},

	sundry: {
		desc: "not sure where should I put them",
	},
	
	symbols: {
	},

	technology: {
	},

	war: {
		desc: "wars & battles & weapons",
	},


//move to major after you add more questions 
	fantasy: {
		hidden: true, //not enough questions and categories 	
	},
	
	nature: {
		desc: "plants and ecosystems", 
		hidden: true, //not enough questions and categories 	
	},

 
	


//MINOR 
	anatomy: {
		desc: "human anatomy",
		hidden: true,
	},

	empire: {
		hidden: true,
	},

	heraldry: {
		desc: "coats of arms",
		hidden: true, //not enough questions and categories 
	},
	
	popculture: {
		hidden: true,
	},
	
	language: {
		hidden: true,
	},

	ships: {
		hidden: true,
	},
	
	sport: {
		hidden: true, //not enough questions and categories 
	},
	
	
	
	
//REGION-RELATED 
	africa: {
	},
	
	america: {
		desc: "American countries with exception of the United States",
	},

	asia: {
	},

	arab: {
		name: "middle-east",
		desc: "north africa & middle-east & Arab conquest",
		hidden: true,
	},

	ceu: {
		name: "Central Europe",
		desc: "related to Germany, Czech Republic, Poland, Austria-Hungary or Holy Roman Empire",
	},

	europe: {
		desc: "related to European countries with exception of United Kingdom",
	},

	france: {
		desc: "related to France, French laguage, history or sphere",
	},

	germany: {
		desc: "related to Germany or German-speaking countries",
	},
	
	islands: {
	
	},

	japan: {
		hidden: true,
	},

	oceania: {
		hidden: true,
	},
	
	russia: {
		hidden: true,
	},

	uk: {
		name: "United Kingdom",
		desc: "related to UK or former British Empire",
	},

	usa: {
		name: "USA",
	},

	scandinavia: {
		hidden: true,
	},
	


	
};